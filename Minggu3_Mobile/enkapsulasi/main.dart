import 'lingkaran.dart';

void main(List<String> args) {
  double luasLingkran;
  Lingkaran lingkaran = new Lingkaran();
  lingkaran.setJari(-10);

  luasLingkran = lingkaran.hitungLuas();
  print(luasLingkran);
}